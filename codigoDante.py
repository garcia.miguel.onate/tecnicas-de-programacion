class DatosPaciente:
    NACIONALIDAD='mexicana'
    ESTADO:'CDMX'
    CLINICA='Angeles'
    COLONIA='Toriello'
    DELEGACION='Tlalpan'
    def _init_(self,diagnostico,numfamiliares,sintomas,sexo,edad,peso):
        self.diagnostico:'Negativo'
        self.numfamiliares=numfamiliares
        self.numfamiliares_consintomas=numfamiliares_consintomas
        self.sintomas=sintomas
        self.sexo=sexo
        self.nombre=None
        self.edad=edad
        if type (peso)== type ("s"):
            self.peso=peso
        else:
            raise ValueError("EL PESO DEBE SER DE TIPO CADENA")
    def NombrePaciente(self):
        self.nombre=input('Cambiar nombre de %s'%self.nombre)
    def EdadPaciente(self):
        self.edad=input('Cambiar edad de %d'%self.edad)
    def PesoPaciente(self):
        self.peso=input('Cambiar peso de %d'%self.peso)
    def DiagnosticoPaciente (self):
        self.diagnostico=input('Cambiar diagnostico de %s'%self.diagnostico)
    def FamiliaresPaciente(self):
        self.numfamiliares=input('Cambiar numero de familiares de %s'%self.numfamiliares)